﻿namespace ProductsDemo.Controllers
{
    public class Product
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}