﻿using ProductsDemo.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsDemo
{
    public class ProductResponse
    {
        public string ResponseType { get; set; }
        public ProductResponse()
        {
            ResponseType = this.GetType().Name;
        }
    }
    public class SaveProductOK: ProductResponse
    {
        public Product Product { get; set; }
    }
    public class SaveProductInvalidID: ProductResponse
    {
        public Product Product { get; set; }
    }
    public class ResponseIDOK:ProductResponse
    {

    }
    public class ProductIDExists: ProductResponse
    {

    }
}
